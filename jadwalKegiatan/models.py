from django.db import models
# from django.forms import ModelForm

class JadwalKegiatan(models.Model):
    kegiatan = models.CharField(max_length=27)
    kategori = models.CharField(max_length=27)
    tempat = models.CharField(max_length=27)
    jam = models.TimeField()
    tanggal = models.DateTimeField()
    
    def __str__(self):
        return self.kegiatan


# Create your models here.
