from django.urls import path
from . import views

app_name = 'jadwalKegiatan'

urlpatterns = [
    path('', views.jadwal, name='jadwal'),
    path('delete/<int:delete_id>', views.delete, name='delete')
]