from django import forms
from .models import JadwalKegiatan

class JadwalKegiatanForm(forms.ModelForm):
    class Meta:
      model = JadwalKegiatan
      fields = '__all__'
      widgets = {
        'tanggal' : forms.SelectDateWidget()
      }
