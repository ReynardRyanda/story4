# Generated by Django 2.2.5 on 2019-10-08 13:24

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='JadwalKegiatan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kegiatan', models.CharField(max_length=27)),
                ('kategori', models.CharField(max_length=27)),
                ('tempat', models.CharField(max_length=27)),
                ('jam', models.TimeField()),
                ('tanggal', models.DateTimeField()),
            ],
        ),
    ]
