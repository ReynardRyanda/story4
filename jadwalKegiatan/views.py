from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from .models import JadwalKegiatan
from .forms import JadwalKegiatanForm

# Create your views here.
def jadwal(request):
  form = JadwalKegiatanForm(request.POST or None)
  jadwal = JadwalKegiatan.objects.all()

  if request.method == "POST":
    if form.is_valid():
      form.save()
      return redirect('/jadwal')

  return render(request, 'jadwal.html', {
    'form' : form,
    'jadwal' : jadwal
  })

def delete(request, delete_id):
  JadwalKegiatan.objects.filter(id=delete_id).delete()
  return redirect('jadwal:jadwal')